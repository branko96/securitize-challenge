import axios from 'axios';
import {
  BASE_PATH_ETHERSCAN_API,
  BASE_PATH_NEST_API,
} from '../constants/paths';
import { config } from '../config';
import { isOldWallet } from '../utils/walletHelper';
import { ethers } from 'ethers';
import { Wallet } from '../interfaces/wallet';

export const getWalletTransactions = async (walletAddress: string) => {
  const url = `${BASE_PATH_ETHERSCAN_API}?module=account&action=txlist&address=${walletAddress}&startblock=0&endblock=99999999&page=1&offset=10&sort=asc&apikey=${config.apiKey}`;
  return await axios.get(url).then(response => response.data);
};

export const getWalletBalance = async (walletAddress: string) => {
  const url = `${BASE_PATH_ETHERSCAN_API}?module=account&action=balance&address=${walletAddress}&tag=latest&apikey=${config.apiKey}`;
  return await axios.get(url).then(response => response.data);
};

export const createWallet = async (walletAddress: string) => {
  const payload = { address: walletAddress };
  return await axios.post(`${BASE_PATH_NEST_API}/wallet/create`, payload);
};

export const getWallets = async (favorite?: boolean) => {
  let response;
  if (favorite) {
    response = axios.get(`${BASE_PATH_NEST_API}/wallet?orderBy=favorite`);
  } else {
    response = axios.get(`${BASE_PATH_NEST_API}/wallet`);
  }
  return response.then(resp => resp.data.data);
};
export const getWalletById = async (walletId: string) => {
  return await axios
    .get(`${BASE_PATH_NEST_API}/wallet/${walletId}`)
    .then(response => response.data.data);
};
export const getWalletInfo = async (walletId: string) => {
  const wallet: Wallet = await getWalletById(walletId);
  const walletInfo = await getWalletTransactions(wallet?.address);
  let old = false;
  let ethValue = '0';

  if (walletInfo.status === '1' && walletInfo.result.length > 0) {
    const unixTime = walletInfo.result[0].timeStamp;
    old = isOldWallet(unixTime);
  }

  const { status, result } = await getWalletBalance(wallet?.address);
  if (status === '1') {
    ethValue = ethers.utils.formatEther(result);
  }
  return {
    ...wallet,
    balance: Number(ethValue),
    old,
  };
};

export const favoriteWallet = async (walletId: string, favorite: boolean) => {
  const payload = { favorite };
  return await axios.put(`${BASE_PATH_NEST_API}/wallet/${walletId}`, payload);
};
