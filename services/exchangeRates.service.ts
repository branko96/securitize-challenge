import axios from 'axios';
import { BASE_PATH_NEST_API } from '../constants/paths';

export const getExchangeRates = async () => {
  const response = axios.get(`${BASE_PATH_NEST_API}/exchange-rates`);
  return response.then(resp => resp.data.data);
};

export const updateExchangeRate = async (
  exchangeRateId: string | undefined,
  value: number,
) => {
  if (!exchangeRateId) {
    return;
  }
  const payload = { value };
  const response = axios.put(
    `${BASE_PATH_NEST_API}/exchange-rates/${exchangeRateId}`,
    payload,
  );
  return response.then(resp => resp.data.data);
};
