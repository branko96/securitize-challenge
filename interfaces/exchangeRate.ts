export interface ExchangeRate {
    currency: string;
    value: number;
    _id: string;
    createdAt: Date;
}
