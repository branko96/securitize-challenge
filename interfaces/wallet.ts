export interface Wallet {
    address: string
    old?: boolean
    favorite: boolean
    balance?: number
    _id: string
}
