function diff_years(dt2: Date, dt1: Date) {
  let diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= 60 * 60 * 24;
  return Math.abs(Math.round(diff / 365.25));
}

export const isOldWallet = (unixTime: number) => {
  let old: boolean = false;
  const date = new Date(unixTime * 1000);
  const today = new Date();
  const age = diff_years(date, today);
  if (age > 1) {
    old = true;
  }
  return old;
};
