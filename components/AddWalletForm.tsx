import { useState } from 'react';

interface AddWalletProps {
  addWallet: (wallet: string) => void;
}

const AddWallet = ({ addWallet }: AddWalletProps) => {
  const [walletAddress, setWalletAddress] = useState('');
  const onAddWallet = async () => {
    if (!walletAddress) {
      return;
    }
    await addWallet(walletAddress);
    setWalletAddress('');
  };
  return (
    <div className="content-center">
      <div className="max-w-sm rounded overflow-hidden shadow-lg">
        <div className="px-6 py-4">
          <div className="font-bold text-xl mb-2">Wallet Address</div>
        </div>
        <div className="px-6 pt-4 pb-2">
          <input
            className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            type="text"
            value={walletAddress}
            placeholder="Wallet Address"
            onChange={e => setWalletAddress(e.target.value)}
          />
          <div className="content-center">
            <button
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
              onClick={onAddWallet}>
              Save
            </button>
          </div>
        </div>
      </div>
      {/*<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
                Wallet Address
            </label>
            <input
                className="shadow appearance-none rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                value={walletAddress}
                placeholder="Wallet Address"
                onChange={(e) => setWalletAddress(e.target.value)}
            />
            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={onAddWallet}>
                Save
            </button>*/}
    </div>
  );
};

export default AddWallet;
