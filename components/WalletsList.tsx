import Loading from './Loading';
import { Wallet } from '../interfaces/wallet';

interface WalletsListProps {
  wallets: Wallet[];
  goToWalletDetails: (walletId: string) => void;
  onFavoriteWallet: (wallet: Wallet) => void;
  onOrderByFavorite: (favorite?: boolean) => void;
  orderByFavorite: boolean;
  loading: boolean;
}

const WalletsList = ({
  loading,
  wallets,
  goToWalletDetails,
  onFavoriteWallet,
  onOrderByFavorite,
  orderByFavorite,
}: WalletsListProps) => {
  return (
    <div className="w-1/2">
      {loading ? (
        <Loading />
      ) : (
        <table className="w-full">
          <thead>
            <tr>
              <th>Address</th>
              <th>
                <span
                  onClick={() => onOrderByFavorite()}
                  className="cursor-pointer">
                  {orderByFavorite ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      viewBox="0 0 20 20"
                      fill="currentColor">
                      <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      viewBox="0 0 20 20"
                      fill="currentColor">
                      <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h7a1 1 0 100-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM15 8a1 1 0 10-2 0v5.586l-1.293-1.293a1 1 0 00-1.414 1.414l3 3a1 1 0 001.414 0l3-3a1 1 0 00-1.414-1.414L15 13.586V8z" />
                    </svg>
                  )}
                </span>
              </th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {wallets.map((wallet: Wallet, index: number) => (
              <tr key={`wallet_${index}`}>
                <td>
                  <h1>{wallet.address}</h1>
                </td>
                <td>
                  <span
                    onClick={() => onFavoriteWallet(wallet)}
                    className="cursor-pointer">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-6 w-6"
                      fill={wallet.favorite ? 'currentColor' : 'none'}
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}>
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                      />
                    </svg>
                  </span>
                </td>
                <td>
                  <button
                    className="w-full bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    onClick={() => goToWalletDetails(wallet._id)}>
                    Details
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default WalletsList;
